﻿using System;

namespace WpfApp_Demo
{
    static class Transforms
    {
    /*  static double Dmap;
        static double Dscr;
        static double Amap;
        static double Ascr;
        static double MSR; // Map to Screen Ratio
        static double RA; // coord system rotation angle- from screen to map */

        public static void OGZ(out double DD, out double AA, double Xa, double Xb, double Ya, double Yb)
        {
            double dX = Xb - Xa;
            double dY = Yb - Ya;

            DD = Math.Sqrt(dX * dX + dY * dY);
            if (!(DD == 0)) AA = Math.Acos(dX / DD); else AA = 0;
            if (dY < 0) AA = 2 * Math.PI - AA;
        } // OGZ

         public static void FindRotationParams(out double ra, out double msr,    // rotation angle, map to screen ratio
                                double x1_map, double y1_map, double x1_scr, double y1_scr,
                                double x2_map, double y2_map, double x2_scr, double y2_scr)
        {
            double d_map, d_scr, A_map, A_scr;
            OGZ(out d_map, out A_map, x2_map, x1_map, y2_map, y1_map);
            OGZ(out d_scr, out A_scr, x2_scr, x1_scr, y2_scr, y1_scr);
            ra = A_map - A_scr;
            msr = d_map / d_scr;
          

        } //FindRotationParams

        public static void DoCrdConvert(out double X_converted, out double Y_converted,
                                double X0_nod, double Y0_nod, double RA,   // 
                                double Xconv_nod, double Yconv_nod,
                                double X_to_conv, double Y_to_conv)
        {
            double dX = X_to_conv - X0_nod; double dY = Y_to_conv - Y0_nod;
            X_converted = dX * Math.Cos(RA) + dY * Math.Sin(RA) + Xconv_nod;
            Y_converted = - dX * Math.Sin(RA) + dY * Math.Cos(RA) + Yconv_nod;
        }

    }
}
