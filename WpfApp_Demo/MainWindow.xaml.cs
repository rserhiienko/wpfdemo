﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace WpfApp_Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Coordinates' transformation parameters:
        static double MSR; // Map-to-Screen Ratio
        static double RA; // coord system rotation angle- from screen to map
        static double X0_nod; static double Y0_nod;
        static double Xconv_nod; static double Yconv_nod;
        static int clbEllipsRadius = 8;
        // tokens
        static bool MapIsLoaded = false;
        static bool MapIsCalibrated = false;
        static bool CalibrationMode = false;
        Point? lastMousePositionOnTarget;
        Point[] clbPoint = new Point[3];

        int x_cp1;
        int y_cp1;
        int x_cp2;
        int y_cp2;
        int x_cp3;
        int y_cp3;

        Point zoomMousePoint = new Point();
        Point scrollMousePoint = new Point();
        double hOff = 1;
        double vOff = 1;
        double temp_X;
        double temp_Y;
        double X_map, Y_map, X_scr, Y_scr;

        public MainWindow()
        {
            InitializeComponent();
            PreviewMouseWheel += canImage_PreviewMouseWheel;

            svImage.PreviewMouseLeftButtonDown += svImage_PreviewMouseLeftButtonDown;
            PreviewMouseMove += svImage_PreviewMouseMove;
            PreviewMouseLeftButtonUp += svImage_PreviewMouseLeftButtonUp;
            btnLoad.Click += btnLoad_Click;
            btnDoCalibration.Click   += btnDoCalibration_Click;
            btnCalibrationMode.Click += btnCalibrationMode_Click;
            ZoomSlider.ValueChanged += ZoomSlider_ValueChanged;
            MouseRightButtonDown  +=  MouseRightBtn_Click;
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.bmp;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "BMP (*.bmp)|*.bmp|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
                MapIsLoaded = true;
               tempX.Content = Convert.ToString(imgPhoto.Source.Height);
                tempY.Content = imgPhoto.Source.Width;

                MenuItemCP1.IsEnabled = true;
                MenuItemCP2.IsEnabled = true;
                MenuItemCP3.IsEnabled = true;
            }
            e.Handled = true;
        }

        private void canImage_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            double ScaleRate = 0.01;
            if (Keyboard.Modifiers != ModifierKeys.Control)
            {
                svImage.IsEnabled = true;
                return;
            }
            lastMousePositionOnTarget = Mouse.GetPosition(null);
            svImage.IsEnabled = false;
            if (e.Delta > 0)
            {
                ZoomSlider.Value += ScaleRate;
            }

            else if (e.Delta < 0)
            {
                ZoomSlider.Value -= ScaleRate;
             }
            canImage.Height = 3350 * ZoomSlider.Value;
            canImage.Width = 3350 * ZoomSlider.Value;
            svImage.IsEnabled = true;

            // this block intended to keep center of zooming to the mouse pointer
            if (svImage.IsMouseOver)
              {
                  zoomMousePoint = Mouse.GetPosition(null);
                  double tempX = zoomMousePoint.X;
                  double tempY = zoomMousePoint.Y;
                  hOff = svImage.HorizontalOffset;
                  vOff = svImage.VerticalOffset;

                  svImage.ScrollToHorizontalOffset(hOff + (hOff + tempX) * Math.Sign(e.Delta) / 50 / ZoomSlider.Value);
                  svImage.ScrollToVerticalOffset(vOff + (vOff + tempY) * Math.Sign(e.Delta) / 50 / ZoomSlider.Value);
               
              }

        }


        private void ScreenToImgCrdTransform(double Xscr, double Yscr, double horOffset, double vertOffset, double scaleFactor,
                                            out double Ximg, out double Yimg)
        {
            Ximg = (Xscr + horOffset) / scaleFactor;
            Yimg = (Yscr + vertOffset) / scaleFactor;
        }

        private void DrawCircle( double X_img, double Y_img)
        {
            Ellipse ellipse1 = new Ellipse();
            SolidColorBrush clbPointBrush = new SolidColorBrush(Color.FromRgb(23, 216, 39));
            ellipse1.Stroke = clbPointBrush;
            ellipse1.Width = clbEllipsRadius;
            ellipse1.Height = clbEllipsRadius;

            Canvas.SetLeft(ellipse1, (X_img - clbEllipsRadius / 2));
            Canvas.SetTop(ellipse1, (Y_img - clbEllipsRadius / 2));

            canImage.Children.Add(ellipse1);
        }


        private void svImage_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (canImage.IsMouseOver)
            {
                // Actions to prepare scrolling by dragging
                scrollMousePoint = e.GetPosition(svImage);
                hOff = svImage.HorizontalOffset;
                vOff = svImage.VerticalOffset;

                temp_X = scrollMousePoint.X;
                temp_Y = scrollMousePoint.Y;

                svImage.CaptureMouse();
                svImage.Cursor = Cursors.SizeAll;

                // Code below included for testing reasons
                ScreenToImgCrdTransform(temp_X, temp_Y, hOff, vOff, ZoomSlider.Value, out X_scr, out Y_scr);
                tempX.Content = (int)(X_scr); // screen coordinate X with offset and zoom counting
                tempY.Content = (int)(Y_scr); // screen coordinate X with offset  and zoom counting
                e.Handled = true;

            }
            else e.Handled = false;
            
        }
        
        // Moving the image by mouse with pressed leat button (mouse is captured!)
        private void svImage_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (svImage.IsMouseCaptured)
            {
                svImage.ScrollToHorizontalOffset(hOff + (scrollMousePoint.X - e.GetPosition(svImage).X));
                svImage.ScrollToVerticalOffset(vOff + (scrollMousePoint.Y - e.GetPosition(svImage).Y));
            }
        }


        private void svImage_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            svImage.ReleaseMouseCapture();
            svImage.Cursor = Cursors.Arrow;
        }


        private void scrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            svImage.ScrollToHorizontalOffset(svImage.HorizontalOffset + e.Delta);
        }

    
        private void btnDoCalibration_Click(object sender, RoutedEventArgs e) {
            bool isConverted = true;
            if (MapIsLoaded) { 
            try
            {
                x_cp1 = Convert.ToInt32(Xcp1.Text);
                y_cp1 = Convert.ToInt32(Ycp1.Text);
                x_cp2 = Convert.ToInt32(Xcp2.Text);
                y_cp2 = Convert.ToInt32(Ycp2.Text);
                x_cp3 = Convert.ToInt32(Xcp3.Text);
                y_cp3 = Convert.ToInt32(Ycp3.Text);
            }
            catch (System.FormatException ex)
            {
                MessageBox.Show("Перевірте введення координат точок калібрування");
                isConverted = false;
            }

            catch (System.NullReferenceException ex)
            {
                MessageBox.Show("Перевірте вибір трьох точок калібрування");
                isConverted = false;
            }
            catch (System.Exception ex) {
                MessageBox.Show("Невідома помилка на етапі калібрування");
                isConverted = false;
            };
            }
            if (isConverted) {
                Transforms.FindRotationParams(out RA, out MSR,    // rotation angle, map to screen ratio
                    x_cp1, y_cp1, clbPoint[0].X, clbPoint[0].Y,
                    x_cp2, y_cp2, clbPoint[1].X, clbPoint[1].Y);
                if (RA < 0) RA = RA + 2 * Math.PI;

                Transforms.FindRotationParams(out double tempRA, out double tempMSR,    // rotation angle, map to screen ratio
                    x_cp2, y_cp2, clbPoint[1].X, clbPoint[1].Y,
                    x_cp3, y_cp3, clbPoint[2].X, clbPoint[2].Y);
                if (tempRA < 0) tempRA = tempRA + 2 * Math.PI;

                RA = (RA + tempRA) / 2;
                MSR = (MSR + tempMSR) / 2;

                X0_nod = clbPoint[0].X * MSR; Y0_nod = clbPoint[0].Y * MSR;
                Xconv_nod = x_cp1; Yconv_nod = y_cp1;

                if ((Math.Abs(RA - tempRA) < 0.1) & (Math.Abs(MSR - tempMSR) < 0.1))
                {
                    MapIsCalibrated = true;
                    tempX.Content = MSR;
                    tempY.Content = RA;
                  //  clbPointNr = 0;
                    btnDoCalibration.Content = "Calibrated";
                    exCalibrationPanel.Header = "Map is calibrated";
                    CalibrationMode = false;
                    MenuItemGrids.IsEnabled = true;
                }
                else MessageBox.Show("Перевірте калібрування!");
            } // finding rotation parameters
        }

        private void ZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e) {
          
        }

        private void btnCalibrationMode_Click(object sender, RoutedEventArgs e) {
            if (MapIsLoaded) { 
            CalibrationMode = true;
            tempX.Content = "CalibrMode";
            }
            else MessageBox.Show("Please load the map!");
        }


        Point MouseRightBtnClickPoint = new Point();
        private void MouseRightBtn_Click(object sender, MouseButtonEventArgs e)
        {
            MouseRightBtnClickPoint = e.GetPosition(svImage);
        }

        private void SetCP1_Click(object sender, RoutedEventArgs e)
        {
            double X_t = MouseRightBtnClickPoint.X;
            double Y_t = MouseRightBtnClickPoint.Y;

            ScreenToImgCrdTransform(MouseRightBtnClickPoint.X, MouseRightBtnClickPoint.Y, 
                                    svImage.HorizontalOffset, svImage.VerticalOffset, ZoomSlider.Value, out X_t, out Y_t);
            clbPoint[0].X = X_t;
            clbPoint[0].Y = Y_t;

            DrawCircle(X_t, Y_t);

            TextBlock cpNr = new TextBlock() { Text = "1" };
            canImage.Children.Add(cpNr);
            Canvas.SetLeft(cpNr, (X_t - clbEllipsRadius / 2 + 7));
            Canvas.SetTop(cpNr, (Y_t - clbEllipsRadius / 2 - 12));

          //  MessageBox.Show("Перевірте введення координат точок калібрування " + X_t.ToString());
        }

        private void SetCP2_Click(object sender, RoutedEventArgs e)
        {
            double X_t = MouseRightBtnClickPoint.X;
            double Y_t = MouseRightBtnClickPoint.Y;

            ScreenToImgCrdTransform(MouseRightBtnClickPoint.X, MouseRightBtnClickPoint.Y, 
                                    svImage.HorizontalOffset, svImage.VerticalOffset, ZoomSlider.Value, out X_t, out Y_t);
            clbPoint[1].X = X_t;
            clbPoint[1].Y = Y_t;

            DrawCircle(X_t, Y_t);

            TextBlock cpNr = new TextBlock() { Text = "2" };
            canImage.Children.Add(cpNr);
            Canvas.SetLeft(cpNr, (X_t - clbEllipsRadius / 2 + 7));
            Canvas.SetTop(cpNr, (Y_t - clbEllipsRadius / 2 - 12));
        }

        private void SetCP3_Click(object sender, RoutedEventArgs e)
        {
            double X_t = MouseRightBtnClickPoint.X;
            double Y_t = MouseRightBtnClickPoint.Y;

            ScreenToImgCrdTransform(MouseRightBtnClickPoint.X, MouseRightBtnClickPoint.Y, 
                                    svImage.HorizontalOffset, svImage.VerticalOffset, ZoomSlider.Value, out X_t, out Y_t);

            clbPoint[2].X = X_t;
            clbPoint[2].Y = Y_t;

            DrawCircle(X_t, Y_t);

            TextBlock cpNr = new TextBlock() { Text = "3" };
            canImage.Children.Add(cpNr);
            Canvas.SetLeft(cpNr, (X_t - clbEllipsRadius / 2 + 7));
            Canvas.SetTop(cpNr, (Y_t - clbEllipsRadius / 2 - 12));
        }

        private void ShowGrids_Click(object sender, RoutedEventArgs e)
        {
            double X_t = MouseRightBtnClickPoint.X;
            double Y_t = MouseRightBtnClickPoint.Y;

            ScreenToImgCrdTransform(MouseRightBtnClickPoint.X, MouseRightBtnClickPoint.Y,
                                    svImage.HorizontalOffset, svImage.VerticalOffset, ZoomSlider.Value, out X_t, out Y_t);

            if (MapIsCalibrated)
            {
                Transforms.DoCrdConvert(out X_map, out Y_map,
                                 X0_nod, Y0_nod, -RA,   // 
                                 Xconv_nod, Yconv_nod,
                                 X_t * MSR, Y_t * MSR);
                tempX.Content = (int)(X_map);
                tempY.Content = (int)(Y_map);
            }

            DrawCircle(X_t, Y_t);
   
        }

    }
}
